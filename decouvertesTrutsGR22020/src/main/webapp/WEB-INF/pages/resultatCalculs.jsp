<%@ taglib prefix="s" uri="/struts-tags" %>
<%--
  Created by IntelliJ IDEA.
  User: sould
  Date: 04/02/2020
  Time: 10:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><s:text name="resultat.titre"></s:text></title>
</head>


<body>
<p>
    <s:text name="resulat.res"></s:text>
    <s:property value="operande1"></s:property>
    <s:property value="operation"></s:property>
    <s:property value="operande2"></s:property>
    <s:text name="resultat.est"></s:text>
    <s:property value="resultat"></s:property>

</p>
</body>



</html>
