<%@ taglib prefix="s" uri="/struts-tags" %>


<html>
<head>
    <title><s:text name="accueil.titre"> </s:text></title>
</head>
<body>
<ul>
    <li><s:a action="connexion"><s:text name ="accueil.connexion"/></s:a></li>
    <li><s:a action="calculatrice"><s:text name ="accueil.calculatrice"/></s:a></li>
    <li><s:a action="calculatriceDynamique"><s:text name ="accueil.dynamique"/></s:a></li>
    <li><s:a action="etudiant"><s:text name ="accueil.etudiants"/></s:a></li>
</ul>
</body>
</html>