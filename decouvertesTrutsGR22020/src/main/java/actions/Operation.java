package actions;

import com.opensymphony.xwork2.ActionSupport;
import modele.CalculatriceDynamiqueDuFutur;
import modele.CalculatriceDynamiqueDuFuturImpl;

import java.util.Collection;

public class Operation extends Environnement{
    public Collection<String> getLesOperations() {
        return getCalculatriceDynamiqueDuFutur().getOperations();
    }
}
