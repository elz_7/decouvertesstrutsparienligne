package actions;

import com.opensymphony.xwork2.ActionSupport;
import modele.Etudiant;
import modele.FacadeModele;

import java.util.Collection;

public class lesEtudiants  extends ActionSupport {
    private FacadeModele facadeModele =new FacadeModele();


    public Collection<Etudiant> getLesEtudiants (){
       return facadeModele.getEtudiants();
    }
}
