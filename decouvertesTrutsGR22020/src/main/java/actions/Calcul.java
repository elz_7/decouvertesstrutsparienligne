package actions;

import com.opensymphony.xwork2.ActionSupport;

import java.awt.*;

public class Calcul extends ActionSupport {
    private double operande1;
    private double operande2;
    private String operation;
    private double resultat;

    public double getOperande1() {
        return operande1;
    }

    public String getOperation() {
        return operation;
    }

    public double getOperande2() {
        return operande2;
    }

    public double getResultat() {
        return resultat;
    }

    public void setOperande1(double operande1) {
        this.operande1 = operande1;
    }

    public void setOperande2(double operande2) {
        this.operande2 = operande2;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public void setResultat(double resultat) {
        this.resultat = resultat;
    }

    @Override
    public String execute() throws Exception {
      //  if(operation ==null) return INPUT;
        switch (operation){
            case "addition":
                resultat=operande1+operande2;
                break;
            case "soustraction":
                resultat=operande1-operande2;
                break;
            case "multiplication":
                resultat=operande1*operande2;
                break;
            case "division":
                resultat=operande1/operande2;
                break;

        }
        return SUCCESS;
    }



    @Override
    public void validate(){
        if(operation!=null) {
            if (operation.equals("division") && operande2 == 0.0) {
                addFieldError("operation", getText("resultat.erreurDivision"));
            }
        }
    }

}
