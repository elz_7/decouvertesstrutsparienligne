<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="list" uri="/struts-tags" %>
<%--
  Created by IntelliJ IDEA.
  User: o2164634
  Date: 28/01/2020
  Time: 11:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><s:text name="calculatrice.titre"></s:text></title>
</head>
<body>



<s:form action="calcul">
    <s:textfield name="operande1" key="calculatrice.operande1"/>
    <s:textfield name="operande2" key="calculatrice.operande2"/>
    <s:select list="{'addition','soustraction','division','multiplication'}" name="operation" key="accueil.operation"/>
    <s:submit name="bouton"></s:submit>
</s:form>
</body>
</html>
